# tkc-doc

Project with Gitlab Pages to put Thunder Kubernetes Controller (i.e. TKC) files for end users to download. Including:
- TKC Custom Resource Definitation files.
- TKC Helm Charts.

